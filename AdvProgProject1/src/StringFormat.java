/**
 * This class gives methods to format strings in various ways.
 * @author Ivan J. Miranda Marrero
 *
 */
public class StringFormat
{
	/**
	 * This static method formats the original string with a given pattern written in "x" and "-", and no other characters.
	 * Each "x" gets replaced by the corresponding character of the original string and each "-" stays the same as in the pattern given. 
	 * @param original the string to be formatted.
	 * @param pattern the pattern that the string should imitate.
	 * @return the formatted string.
	 */
	public static String patternImitator(String original, String pattern)
	{
		char[] patternString = pattern.toCharArray(); //Converts the original string to an array containing the same sequence of characters.
		
		int patternIndex = 0; //Counts the position in the pattern array. It is declared outside the for loop because it is needed
							  //to return the new string correctly.
		
		/* The following controls the formatting of the string. OriginalIndex counts the position in the original string. The loop ends 
		 * when either the "pattern" index reaches to the end of the array or or the original string index reaches the end of the string.
		 * For each x replaced, the original string's index increases by one. The pattern index increases by one with each iteration.*/
	
		for (int originalIndex = 0; (patternIndex < pattern.length()) && (originalIndex < original.length()); patternIndex++)
		{
			//TODO if you have nothing left to do, add comparisons to put a question mark '?' for an unrecognized character in the formatting pattern as defined in the class explanation.
			if (patternString[patternIndex] == 'x') //Checks if the character at "patternIndex" is an x.
			{
				patternString[patternIndex] = original.charAt(originalIndex); //Replaces the character at the same position with the 
				originalIndex++;											  //corresponding character of the original string and advances
																			  //the position of the original string.
			} //End if.
			
		} //End for.
	
		return new String(patternString, 0, patternIndex); //Converts the array to a new string up to the last character modified.
														   //The patternIndex is needed to prevent extra "x" in the case that we have
														   //less characters in the original string than "x" in the pattern string.
	} //End method.
	
	/**
	 * This static method reverses a string.
	 * @param original the string to reverse.
	 * @return the reversed string.
	 */
	public static String stringReverse(String original)
	{
		String reversedString = ""; //Reversed string starts empty.
		
		for (int originalIndex = 0; originalIndex < original.length(); originalIndex++)
		{
			reversedString = original.charAt(originalIndex) + reversedString; //Example: "olleH" will be reversed as follows: 
		}																	  //o, lo, llo, ello, Hello. It takes the character first
		           															  //and then concatenates it with "reversedString".
		return reversedString; 
	}
	
} //End class
